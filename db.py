import sqlite3

def connectDatabase(name = "Withings.s3db"):
    conn = sqlite3.connect(name)
    c = conn.cursor()
    return conn, c

def createTable(c, tableName, cols):
    c.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='{}'".format(tableName))
    if len(c.fetchall()) > 0:
        return False

    cols = ["'{}' '{}'".format(name, dataType) for name, dataType in cols]
    sql = "CREATE TABLE {} ({})".format(tableName, ', '.join(cols))
    c.execute(sql)

    return True

def addLine(c, tableName, data, **kwargs):
    unique = kwargs.get('unique', None)
    uniqueIndex = kwargs.get('uniqueIndex', -1)
    
    if unique is None:
        sql = ""
    else:
        sql = "SELECT * FROM {} WHERE {} = '{}'".format(tableName, unique, str(data[uniqueIndex]))
    
    if unique is None or len(c.execute(sql).fetchall()) == 0:
        c.execute("INSERT INTO {} VALUES ({})".format(tableName, ','.join(['?' for i in range(len(data))])), data)
        return True
    else:
        return False
