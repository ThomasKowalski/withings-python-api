![Withings.png](https://bitbucket.org/repo/yxL6xR/images/2202128497-Withings.png)

# Pythings : A Withings API for Python #

PyThings is, besides a very bad name for an API, a native Python implementation of the Withings Partner API. It will allow you to make requests on most of endpoints (or implement it yourself easily, without having to handle the OAuth part.)

Since it has been first made for me to be able to get my data back locally, I've included an `examples.py` file that will gather the most common data (activities from your watch, sleep data...) and store it in a SQLite database. The code is very clean (in my opinion) and simple to understand, that's why I encourage you to read the `examples.py` file to fully understand the mechanics of the API. 

Also, if you wish to implement custom endpoints, you should definitely the [OAuth quickstart guide](https://oauth.withings.com/api) on the Withings website, that explains how everything goes. 

I also recommand you use `PyLint` (with or without Visual Studio Code, but the integration with VS Code is actually great) so you can have access to documentation for functions as you type, without having to go back and forth to definitions.

# Dependancies # 

In order to use Pythings, you'll need to install `pyperclip`, a Python module used to easily set and access the paperclip (used to copy the URL for the Web authentication part.)

# Configuration #

You'll need to setup your `settings.json` file with the consumer keys you can get on the [Partner Registration page](https://oauth.withings.com/partner/dashboard).

If possible, use a `settings_user.json` file rather than a `settings.json` file. It does the exact same thing, but in case you'd like to contribute, using a file named `settings.py` will commit your modifications to the virgin file. Which is *not* what you want. Any changes brought to `settings_user.json` will be ignored, though. 

A last thing. If you want to use the `GetIntradayActivity` endpoint, you'll also need to contact Withings for them to enable it on your account.

# To Do #

* Documentation 
* Update GetActivity handler to a better version
* Finish the GetIntradayActivity implementation.
* Add docstrings everywhere.

# FAQ # 

*What is the difference between `config.json` and `settings.json` ?*

The answer is simple: you should never have to mess with `config.json`. It's used to store the keys sent back by the API when you authorize the app, so you don't have to authorize it each time. On the other hand, `settings.json` *has* to be edited by you on your first use (see *Configuration*). 

__Side note:__ if you want to force a relogin, delete the `config.json` file.