import time
import base64
import hashlib
import urllib.parse
import collections
import hmac
import json
import os

import requests

# Dependancies

# PyperClip allows you to easily copy text to the clipboard, independantly of the operating system.
# It's quite useful for debugging purposes, but mainly to copy the OAuth authorization URL to the user's clipboard
import pyperclip

def loadConfig():
    '''
    Will try to load the data from config.json
    If it dosn't exist, return an empty dictionary.
    '''
    if os.path.isfile("config.json"):
        with open("config.json") as f:
            content = f.read()
            if content == '':
                return dict()
            config = json.loads(content)
    else:
        return dict()
    return config

def loadSettings(file = "settings.json"):
    if os.path.isfile("settings_user.json"):
        file = "settings_user.json"

    if os.path.isfile(file):
        with open(file) as f:
            settings = json.loads(f.read())

        key, secret = settings["consumerKey"], settings["consumerSecret"]

        if key == "" or secret == "":
            print("Please enter your details in the settings.json file.")
            raise Exception

        return key, secret

    else:
        print("Please create and enter your details in the " + file + " file.")
        raise Exception

def getNonce(): #to be fixed
    '''
    Returns a random token.
    '''
    return "5287cfe22664b7d4e334ca4e8ea3a2c2"

def getSignature(url, payload, secret):
    '''
    Returns the signature associated to the parameters
    - url     : API-EndpoINTEGER
    - payload : the set of parameters that you want to send
                 If you're always using the same payload, you should be worry of removing the old parameters
                 (example : oauth_callback is needed sometimes, even if it's empty, and sometimes it has to be absent
                            this is quite important since if you leave it, your secret will encrypt a string that has 
                            an unneeded parameter, causing the signature to be wrong
                            If you want to avoid that at all costs, just use a new payload everytime.
                            By the way, the old oauth_signature will automatically be removed.)
    - secret  : the key you want to use for the HMAC. Withings API Reference describes what you have to use for every
                 endpoINTEGER. 
    '''

    # Remove any former signature
    if "oauth_signature" in payload.keys():
        del payload["oauth_signature"]

    # Reorder the parameters
    payload = collections.OrderedDict(sorted(payload.items()))

    # Construct the string
    requestString = ""
    for k, v in payload.items():
        requestString += str(k) + "=" + str(v) + "&"

    requestString = requestString.strip('&')
    requestString = urllib.parse.quote_plus(requestString)
    requestString = "GET&" + urllib.parse.quote_plus(url) + "&" + requestString

    dig = hmac.new((secret).encode('utf-8'), msg = requestString.encode('utf-8'), digestmod = hashlib.sha1).digest()
    signature = base64.b64encode(dig).decode()

    return signature

def getDefaultPayload(consumerKey = loadSettings()[0]):
    '''
    Returns the default payload. 
    It has all the parameters that are used for all enpoints. 
    And... hum... also the oauth_callback parameter, which you shoud remove
        if you were to use the payload on a non-OAuth authorization related
        endpoint.
    '''

    payload = {
        "oauth_nonce" : getNonce(),
        "oauth_consumer_key" : consumerKey,
        "oauth_callback" : "",
        "oauth_signature_method" : "HMAC-SHA1",
        "oauth_timestamp" : int(time.time()),
        "oauth_version" : 1.0
    }

    # Yes, since we generate a hash of the request URL,
    # we need to be sure that the parameters are in the good order.
    # That's why we use an ordered dict. 
    return collections.OrderedDict(sorted(payload.items()))

def firstUse(userid = None, consumerSecret = loadSettings()[1]):
    '''
    The main function, used to generate the codes on the first time. 
    You shouldn't have to edit this one, since it's working 
    like a *fine-tuned machine.*
    If it ever stopped working, please contact me on BitBucket.
    '''

    # Hardcoded because why not.
    requestTokenEndpoint = "https://oauth.withings.com/account/request_token"
    accessTokenEndpoint = "https://oauth.withings.com/account/access_token"
    authorizationEndpoint = "https://oauth.withings.com/account/authorize"

    # Initialize our payload, we'll be using the same until the end of the process.
    payload = getDefaultPayload()

    # Generate the signature for the first request. At that time, we only have the base parameters.
    payload['oauth_signature'] = getSignature(requestTokenEndpoint, payload, consumerSecret + '&')

    # Get the response
    r = requests.get(requestTokenEndpoint, params = payload).text
    
    # Split the response and extract the data
    oauthtoken = r.split('&')[0].split('=')[1]
    oauthtokensecret = r.split('&')[1].split('=')[1]

    # Let's continue to step 2!
    # Which is getting the authorization URL
    payload["oauth_token"] = oauthtoken
    payload["oauth_nonce"] = getNonce()
    payload["oauth_timestamp"] = int(time.time())
    del payload["oauth_signature"]

    payload["oauth_signature"] = getSignature(authorizationEndpoint, payload, consumerSecret + '&' + oauthtokensecret)

    # Get the response
    r = requests.get(authorizationEndpoint, params = payload)
    
    # Copy the URL to clipboard and ask the user to authorize the app
    # Since there is no (easy) way to get the used ID back from the response,
    # We'll need to ask the user to give it to us.
    pyperclip.copy(r.url)
    print("URL has been copied to clipboard!")
    print("Please authorize the application and get the user ID from the address")
    print("After authorization, you'll be redirected to an URL looking like")
    #https://oauth.withings.com/account/authorize?acceptDelegation=true&oauth_consumer_key=60e1d606f04f72e965898e8e557983eb34b5f0146b922c0e7ec4298c441ce&oauth_nonce=58630d5cc081d408a43fce5adc22b481&oauth_signature=VzDWjW%2Frf3sqaOE2Zx6jHsh9pjw%3D&oauth_signature_method=HMAC-SHA1&oauth_timestamp=1487893585&oauth_token=5339fd402c17d843fc0c7755bdb1154edd45f1179cbfbcb8768688cf3b&oauth_version=1.0&userid=12650609
    print("https://thewithingssite/xx/?xx=xx&yy=yy&zz=zz&userid=NNNNNN")
    print("Please enter the NNNNNN")
    userid = input("User ID: ")

    # Regenerate a new signature, and process to the next step
    # Finally, we're gonna get our access tokens.
    del payload["oauth_signature"]
    payload["oauth_timestamp"] = int(time.time())
    payload["oauth_nonce"] = getNonce()
    payload["userid"] = userid

    payload["oauth_signature"] = getSignature(accessTokenEndpoint, payload, consumerSecret + "&" + oauthtokensecret)

    r = requests.get(accessTokenEndpoint, params = payload).text

    # Extract the data and return it
    accessToken = r.split('&')[0].split('=')[1]
    accessTokenSecret = r.split('&')[1].split('=')[1]
    userid = r.split('&')[2].split('=')[1]

    return accessToken, accessTokenSecret, userid

def init():
    '''
    Initialize the script.
    At the end of this function, we should always have access tokens!
    '''
    config = loadConfig()
    # If anything is missing in the config, get new codes.
    if not "accessToken" in config.keys() or not "accessTokenSecret" in config.keys() or not "userid" in config.keys():
        config['accessToken'], config['accessTokenSecret'], config['userid'] = firstUse()

    # Save it for the next time
    with open("config.json", "w") as f:
        f.write(json.dumps(config))
    
    return config

def setSignature(payload, url, secret):
    payload["oauth_signature"] = getSignature(url, payload, secret)

def getData(url, args, kw = None, **kwargs):
    if kw is None:
        kw = kwargs

    payload = getDefaultPayload()
    if "oauth_callback" in payload.keys():
        del payload["oauth_callback"]
    for arg, val in args.items():
        payload[arg] = val
    setSignature(payload, url, kw.get("consumerSecret") + '&' + kw.get("accessTokenSecret"))

    r = requests.get(url, params = payload).json()

    if r["status"] == 601:
        print("Too many requests. Waiting one minute. ({})".format(int(time.time())))
        time.sleep(60)
        return False
    elif r["status"] != 0:
        print("Incorrect status: ", r["status"])
        print(r)
        raise Exception

    return r

def dateToString(d):
    return "{0.year:0>2}-{0.month:0>2}-{0.day:0>2}".format(d)

def canContinueQuerying(data, limit = 60, reset = 60):
    timestamp = int(time.time())
    for item in data:
        if timestamp - item > reset:
            data.pop(item)

    return not (len(data) > limit), data
