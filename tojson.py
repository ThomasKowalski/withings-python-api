import json

import requests

def getLatestScript():
    result = requests.get("https://bitbucket.org/!api/2.0/snippets/ThomasKowalski/aeERB").text
    j = json.loads(result)
    result = requests.get(list(j["files"].values())[0]["links"]["self"]["href"]).text
    with open("sqlitetojson.py", 'w') as f:
        f.write(result)

def toJson(r = False):
    try:
        getLatestScript()

        try:
            import sqlitetojson as s2j
            s2j.export("Withings.s3db")

            if r:
                with open("Withings.s3db.json") as f:
                    return json.loads(f.read())

        except:
            print("Couldn't import SQLite3ToJSON")
    except:
        print("Couldn't get latest version of module.")

if __name__ == "__main__":
    toJson()
