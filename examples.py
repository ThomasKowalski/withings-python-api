import datetime as dt
import time
import json

import withings
import db

def getSleepSummary(params, limit = 0, **kwargs):
    # We don't need noDataLimit here since Withings is giving a "more" field in the response body.
    print("Getting sleep summaries.")

    db.createTable(c, "SleepSummary", (("ID",               "INTEGER"), # I'm not quite sure of what this represents...
                                       ("StartDate",        "INTEGER"),
                                       ("EndDate",          "INTEGER"),
                                       ("WakeUpCount",      "INTEGER"),
                                       ("Awake",            "INTEGER"),
                                       ("Light",            "INTEGER"),
                                       ("Deep",             "INTEGER"),
                                       ("REM",              "INTEGER"),
                                       ("DurationToSleep",  "INTEGER"),
                                       ("DurationToWakeUp", "INTEGER")))

    url = "https://wbsapi.withings.net/v2/sleep" #get / getsummary
    params["action"] = "getsummary"
    
    currentDate = dt.datetime.now()

    endDate = withings.dateToString(currentDate)
    startDate = withings.dateToString(currentDate - dt.timedelta(days = 190))

    more = True
    while currentDate.timestamp() > limit and more:
        params["startdateymd"] = startDate
        params["enddateymd"] = endDate

        raw = withings.getData(url, params, kwargs)

        noDataStreak = 0

        if raw != False:
            if len(raw["body"]["series"]) == 0:
                noDataStreak += 1
                continue
            more = raw["body"]["more"]
            raw = raw["body"]["series"]

            for s in raw:
                data = s["data"]
                db.addLine(c, "SleepSummary", (s['id'],
                                               s['startdate'],
                                               s['enddate'],
                                               data['wakeupcount'],
                                               data['wakeupduration'],
                                               data['lightsleepduration'],
                                               data['deepsleepduration'],
                                               data.get('remsleepduration', -1),
                                               data['durationtosleep'],
                                               data.get('durationtowakeup', -1)), unique = 'ID', uniqueIndex = 0)

        currentDate = dt.datetime.now() - dt.timedelta(days = 190)
        endDate = withings.dateToString(currentDate)
        startDate = withings.dateToString(currentDate - dt.timedelta(days = 190))

def getIntradayActivity(params, limit = 0, noDataLimit = 15, timestampDelta = 24 * 3600, **kwargs):
    print("Getting intraday activity.")

    url = "https://wbsapi.withings.net/v2/measure"
    params["action"] = "getintradayactivity"

    db.createTable(c, "IntradayActivity", (("Timestamp",  "INTEGER"),
                                           ("Duration",   "INTEGER"),
                                           ("Calories",   "INTEGER"),
                                           ("Steps",      "INTEGER"),
                                           ("Distance",   "INTEGER"),
                                           ("Elevation",  "INTEGER"),
                                           ("SleepState", "INTEGER")))

    date = dt.datetime.now() + dt.timedelta(days = 1)

    timestamp = int(date.timestamp()) + timestampDelta

    noDataStreak = 0

    while timestamp > limit and noDataStreak < noDataLimit:
        timestamp -= timestampDelta
        params["startdate"] = timestamp - timestampDelta
        params["enddate"] = timestamp

        raw = withings.getData(url, params, kwargs)

        if raw != False:
            if len(raw["body"]["series"]) == 0:
                noDataStreak += 1
                continue

            raw = raw["body"]["series"]

            for t, s in raw.items():
                db.addLine(c, "IntradayActivity", (t,
                                                   s['duration'],
                                                   s.get('calories', -1),
                                                   s.get('steps', -1),
                                                   s.get('distance', -1),
                                                   s.get('elevation', -1),
                                                   s.get('sleep_state', -1)), unique = 'Timestamp', uniqueIndex = 0)
        else:
            print("Too many requests. ({})".format(int(time.time())))
            timestamp += 24 * 3600

    print("Finished getting intraday activity.")

def getSleep(params, limit = 0, noDataLimit = 5, **kwargs):
    print("Getting sleep data.")

    db.createTable(c, "Sleep", (("Start", "INTEGER"),
                                ("End",   "INTEGER"),
                                ("State", "INTEGER")))

    url = "https://wbsapi.withings.net/v2/sleep" #get / getsummary
    params["action"] = "get"

    timestampDelta = 7 * 24 * 3600
    
    date = dt.datetime.now() + dt.timedelta(days = 1)
    noDataStreak = 0
    date = date.replace(hour = 12, minute = 0)
    timestamp = int(date.timestamp()) + timestampDelta

    while timestamp > limit and noDataStreak < noDataLimit:
        timestamp -= timestampDelta
        params["startdate"] = timestamp - timestampDelta
        params["enddate"] = timestamp

        raw = withings.getData(url, params, kwargs)
        if raw != False:
            if len(raw["body"]["series"]) == 0:
                noDataStreak += 1
                continue
            raw = raw["body"]["series"]

            for s in raw:
                db.addLine(c, "Sleep", (s['startdate'],
                                        s['enddate'],
                                        s['state']), unique = 'Start', uniqueIndex = 0)
        else:
            # We're on an API cooldown, so we go back to the previous timestamp.
            timestamp += timestampDelta
            
def getWorkouts(params, limit = 0, **kwargs):
    print("Getting workout data.")

    db.createTable(c, "Workouts", (("ID",          "INTEGER"), # I'm not quite sure of what this represents...
                                   ("StartDate",   "INTEGER"),
                                   ("Modified",    "INTEGER"),
                                   ("Category",    "INTEGER"),
                                   ("Calories",    "REAL"),
                                   ("EffDuration", "REAL"),
                                   ("Steps",       "INTEGER"),
                                   ("Distance",    "REAL"),
                                   ("MetCumul",    "REAL"), # dafuq is that
                                   ("Intensity",   "INTEGER"),   # it looks like this is in {0, 50, 100}
                                   ("PoolLength",  "REAL"),
                                   ("PoolLaps",    "REAL"),
                                   ("Strokes",     "REAL")))

    url = "https://wbsapi.withings.net/v2/measure" #get / getsummary
    params["action"] = "getworkouts"
    
    currentDate = dt.datetime.now()

    endDate = withings.dateToString(currentDate)
    startDate = withings.dateToString(currentDate - dt.timedelta(days = 190))

    more = True
    while currentDate.timestamp() > limit and more:
        params["startdateymd"] = startDate
        params["enddateymd"] = endDate

        raw = withings.getData(url, params, kwargs)

        if raw != False:
            # We don't use a noDataStreak here since we have a "more" parameter in the response. 
            if len(raw["body"]["series"]) == 0:
                continue

            more = raw["body"]["more"]
            raw = raw["body"]["series"]

            for s in raw:
                data = s["data"]
                db.addLine(c, "Workouts", (s['id'],
                                           s['startdate'],
                                           s['modified'],
                                           s['category'],
                                           data.get('calories', -1),
                                           data.get('effduration', -1),
                                           data.get('steps', -1),
                                           data.get('distance', -1),
                                           data.get('metcumul', -1),
                                           data.get('intensity'),
                                           data.get('pool_length', -1),
                                           data.get('pool_laps', -1),
                                           data.get('strokes', -1)), unique = 'ID', uniqueIndex = 0)

        currentDate = dt.datetime.now() - dt.timedelta(days = 190)
        endDate = withings.dateToString(currentDate)
        startDate = withings.dateToString(currentDate - dt.timedelta(days = 190))

def getActivity(params, limit = 0, noDataLimit = 5, **kwargs):
    print("Getting activity.")

    url = "https://wbsapi.withings.net/v2/measure"
    params["action"] = "getactivity"
    
    date = dt.datetime.now() + dt.timedelta(days = 1)
    dataToAdd = list()

    noDataStreak = 0

    while noDataStreak < noDataLimit and date.timestamp() >= limit:
        date = date - dt.timedelta(days = 1)          # Remove one day from the datetime
        params["date"] = withings.dateToString(date)  # Convert the date to a YYYY-MM-DD
        raw = withings.getData(url, params, kwargs)   # Get the data from the API

        # If we can't get the data, restart the gathering for this day (usually because of cooldowns)
        if not raw:
            # Go back to the day we were trying to get the data for.
            date = date + dt.timedelta(days = 1)
            print(withings.dateToString(date))
            continue

        # Analyze the data, and verify there is data for this day
        d = raw["body"]
        if len(d.keys()) == 0:
            noDataStreak += 1
            continue
        else:
            noDataStreak = 0

        # Add it to the DB-queue
        dataToAdd.append((d["date"], d["timezone"], d["steps"], d["elevation"], d["calories"], d["totalcalories"], d["distance"], d["soft"], d["moderate"], d["intense"]))

    # Add data to the database
    db.createTable(c, "Activity", (("Date",          "TEXT"), 
                                   ("Timezone",      "TEXT"), 
                                   ("Steps",         "INTEGER"),
                                   ("Elevation",     "REAL"), 
                                   ("Calories",      "REAL"), 
                                   ("TotalCalories", "REAL"),
                                   ("Distance",      "REAL"),
                                   ("Soft",          "INTEGER"),
                                   ("Moderate",      "INTEGER"),
                                   ("Intense",       "INTEGER")))

    for d in dataToAdd:
        db.addLine(c, "Activity", d)

if __name__ == "__main__":
    consumerKey, consumerSecret = withings.loadSettings()
    config = withings.init()
    accessToken, accessTokenSecret, userid = config["accessToken"], config["accessTokenSecret"], config["userid"]

    def getCleanParams():
        return {
            "userid" : userid,
            "oauth_token" : accessToken,
        }

    def loadExamplesConfig():
        try:
            with open("examples.json") as f:
                return json.loads(f.read())
        except:
            return { "lastUpdate" : 0 }

    def saveExamplesConfig(data):
        try:
            with open("examples.json", 'w') as f:
                f.write(json.dumps(data))
        except:
            print("Couldn't save last timestamp: {}".format(data["lastUpdate"]))

    # Load examples config (last update)
    examplesConfig = loadExamplesConfig()
    lastTimestamp = examplesConfig["lastUpdate"]

    print("Gathering new data since {}".format(lastTimestamp))
    
    # Connect to the database
    conn, c = db.connectDatabase()

    # Gather various data from the API
    getActivity(getCleanParams(), lastTimestamp, consumerSecret = consumerSecret, accessTokenSecret = accessTokenSecret)
    conn.commit()

    getIntradayActivity(getCleanParams(), lastTimestamp, consumerSecret = consumerSecret, accessTokenSecret = accessTokenSecret)
    conn.commit()

    getWorkouts(getCleanParams(), lastTimestamp, consumerSecret = consumerSecret, accessTokenSecret = accessTokenSecret)
    conn.commit()

    getSleepSummary(getCleanParams(), lastTimestamp, consumerSecret = consumerSecret, accessTokenSecret = accessTokenSecret)
    conn.commit()

    getSleep(getCleanParams(), lastTimestamp, consumerSecret = consumerSecret, accessTokenSecret = accessTokenSecret)
    conn.commit()

    conn.close()
    examplesConfig["lastUpdate"] = int(time.time())
    saveExamplesConfig(examplesConfig)
