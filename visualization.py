import json

import matplotlib.pyplot as plt
import numpy as np

import tojson

# data = tojson.toJson(True)

data = json.loads(open("Withings.s3db.json").read())

x = list()
y = list()

m = 0
M = max([int(x["Calories"]) for x in data["Activity"]])
steps = 10

x = [m + (M - m) * k / steps for k in range(steps)] + [M + 1]
y = [0 for _ in range(len(x))]
N = len(x)
width = 0.35


for i in range(len(x) - 1):
    for j, e in enumerate([x["Calories"] for x in data["Activity"]]):
        if e >= x[i] and e < x[i + 1]:
            y[i] += 1

# for i, e in enumerate(data["Activity"]):
#     x.append(i)
#     y.append(e["Calories"])

print(x, y)


fig, ax = plt.subplots()
rects1 = ax.bar(np.arange(N), y, width, color='r')

# plt.plot(x, y)
plt.show()
